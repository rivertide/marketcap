import requests
import time
import pandas as pd
# import boto3
# import botocore
import random
import io
import json
import datetime
import numpy
from dateutil.relativedelta import *
import os


def get_random_orgs(num):
    s3 = boto3.resource('s3')
    orgID_bucket_code = 'xero-ds-datalake-prod-us-east-1-catchment-i'
    orgId_bucket = s3.Bucket(orgID_bucket_code)
    files_prefix = 'cb/1ed5b18e-e8d4-4e7c-b3e3-3081936e65db/2018/02/16/02/'
    encoding = 'utf-8'

    keys = [i.key for i in orgId_bucket.objects.filter(Prefix=files_prefix)]

    key = random.choice(keys)

    with io.BytesIO() as f:
        orgId_bucket.download_fileobj(key, f)
        df = pd.DataFrame(pd.read_csv(io.BytesIO(f.getvalue()), encoding=encoding))

    orgID_series = df['OrganisationID'].drop_duplicates(inplace=False).sample(num)
    return orgID_series.tolist()
    # 'of type: ' + str(type(of)) + ', df type: ' + str(type(df))
    # of.describe()


def _get_cell(org):
    """
        Gets the cell id for the given organisation.
    """
    routing_url = 'https://routing.service.xero.com/v1/routes/' + org
    route = requests.get(routing_url).json()
    return route['cell']


def _get_cell2(org):
    """
        Gets the cell id for the given organisation.
    """
    routing_url = 'https://routing.service.xero.com/v1/routes/' + org
    route = requests.get(routing_url).json()
    return route


def get_kpi_metrics(org, start_date='2010-01-01', end_date=None):
    """
        Gets the KPI metrics from the KPI service, and returns it in JSON format.
        If the end date is None, today's date will be used instead.
    """
    # cell = _get_cell(org)
    cell = _get_cell2(org)['cell']
    # use today if no end set
    if end_date is None:
        end_date = time.strftime('%Y-%m-%d')
    kpi_url = 'https://kpi.' + cell + '.xero.com/metrics/' + org + '/' + start_date + '/' + end_date
    headers = {'Xero-Tenant-Id': org}
    data = requests.get(kpi_url, headers=headers)
    if data.status_code == 404:
        return None

    return_value = data.json()
    return return_value


def get_org_metrics(org):
    org_classification = requests.get(
        """https://organisation.service.xero.com/organisations/{0}/classification""".format(
            _get_cell2(org)['organisation']), headers={"Xero-Client-Name": "test"})

    # print('\n\n')
    # org_details = requests.get(
    #     """https://organisation.service.xero.com/organisations/{0}""".format(_get_cell2(org)['organisation']),
    #     headers={"Xero-Client-Name": "test"})

    # return {"OrgClassification":json.loads(org_classification.text), "OrgDetails":json.loads(org_details.text)}

    try:
        return json.loads(org_classification.text)
    except:
        return {"industryCodeId": ""}


def get_industry_name(ind, lvl):
    ind_classification = requests.get(
        """http://industry.global.xero.com/v1/codes/?id={0}""".format(ind), headers={"Xero-Client-Name": "test"})

    brk9 = 0


def get_monthly_amounts_pro(data, exclusions=['Constant', 'Days in Month', 'Days in Year']):
    """ Gets just the interesting amounts (values) from the KPI json data.
        Returns a list of dictionaries (can be easily turned into a dataframe).
    """
    top_ratio_num = 0
    top_ratios = []
    top_amounts = []

    all_ratio_num = 0
    all_ratio_num += 1
    all_ratios = []
    all_amounts = []

    all_objects = {}

    def parseExpression(data, run_type='simple'):
        if data is None:
            return ''
        running_text = ''
        for item in data:
            if 'name' in item and 'expression' in item and run_type == 'layers':
                all_ratios.append(
                    {'ratio_name': item['name'], 'expression': parseExpression(item['expression'], 'layers')})
            elif 'expression' in item and run_type == 'deep':
                running_text += '(' + parseExpression(item['expression']) + ')'
            elif 'name' in item:
                running_text += item['name']
            elif 'symbol' in item:
                running_text += ' ' + item['symbol'] + ' '
            else:
                if 'expression' in item:
                    running_text += '(' + parseExpression(item['expression']) + ')'
                else:
                    raise ValueError('Unexpected result.')
        return running_text;

    def parseExpressionValues(data, top_ratio_num, data_date):
        if data is None:
            return ''
        running_text = ''
        for item in data:
            if 'name' in item and 'value' in item:
                all_amounts.append({'top_ratio_num': top_ratio_num, 'data_date': data_date, 'ratio_name': item['name'],
                                    'value': item['value']})
            if 'expression' in item:
                parseExpressionValues(item['expression'], top_ratio_num, data_date)

    if data is None:
        return {}

    if type(data) is dict:
        if len(data) <= 1:
            return {}

    for item in data['items']:
        if item['name'] not in exclusions:
            for granularities in item['granularities']:
                granularity = granularities['name']

                if granularity == 'Monthly':
                    shallow_ratio_expression = parseExpression(granularities['series'][0]['expression'], 'shallow')
                    deep_ratio_expression = parseExpression(granularities['series'][0]['expression'], 'deep')
                    top_ratios.append({'top_ratio_num': top_ratio_num, 'top_ratio_name': item['name'],
                                       'description': item['description'], 'expression': shallow_ratio_expression,
                                       'deep_expression': deep_ratio_expression})
                    top_ratio_num += 1

                    parseExpression(granularities['series'][0]['expression'], 'layers')

                for datepoint in granularities['series']:
                    curr_date = datepoint['date']
                    curr_date = curr_date[:curr_date.rfind('T')]
                    curr_value = datepoint['value']
                    curr_name = item['name']
                    top_amounts.append(
                        {'top_ratio_num': top_ratio_num, 'curr_name': curr_name, 'granularity': granularity,
                         'curr_date': curr_date, 'curr_value': curr_value})

                    if 'expression' in datepoint:
                        parseExpressionValues(datepoint['expression'], top_ratio_num, curr_date)

    # all_objects = { 'top_ratios': top_ratios, 'all_ratios': all_ratios, 'all_amounts': all_amounts}

    all_objects = {}
    all_objects['top_ratios'] = top_ratios
    all_objects['all_ratios'] = all_ratios
    all_objects['top_amounts'] = top_amounts
    all_objects['all_amounts'] = all_amounts

    return all_objects


def getAOrgPro(org, start_date='2010-01-01', end_date=None):
    b = get_monthly_amounts_pro(get_kpi_metrics(org, start_date, end_date))

    if len(b) > 1:
        b['OrganisationId'] = org

    return b


def calculateScores():
    orgs = get_random_orgs(100)
    # orgs = ['3851B3FA-80F6-4CC6-A44C-95CDEC782E65']
    # orgs = [ 'AC9478E7-2E5C-4940-96F4-94D3CCD5F3D5']
    start_date_range = '2017-03-01'
    end_date_range = '2019-02-01'

    def randomDate(start, end, prop):
        stime = time.mktime(time.strptime(start, "%Y-%m-%d"))
        etime = time.mktime(time.strptime(end, "%Y-%m-%d"))
        ptime = stime + prop * (etime - stime)

        return time.strftime("%Y-%m", time.localtime(ptime)) + "-01"

    end_date = randomDate(start_date_range, end_date_range, random.random())

    timestamp_str = str(time.time())

    # org_Results = get_org_metrics(orgs[0])

    # with open('C:\\Temp\\ratios_j-' + timestamp_str + '.json', 'w') as fp:
    #    json.dump(results[0], fp, indent=4, sort_keys=False)

    examine_ratios = ['Retained Earnings', 'Current Liabilities', 'ACCTTYPE/NONCURRENT (Closing)', 'Tangible Net Worth',
                      'ACCTTYPE/BANK (Closing)', \
                      'Total Assets', 'Inventory', 'Net Sales', 'ACCTTYPE/CURRENT (Closing)', 'Fixed Assets',
                      'Net Income', 'Interest Expense', \
                      'ASS.CUR.REC.TRA (Closing)', 'LIA.CUR.PAY.TRA (Closing)',
                      'Earnings Before Interest and Tax (EBIT)']

    num_months = 24
    data_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    start_date = (data_date + relativedelta(years=-2)).strftime("%Y-%m-%d")

    index2 = [(data_date + relativedelta(months=-x)).strftime("%Y-%m-%d") for x in range(1, num_months)]

    i = 0
    results = []
    for org in orgs:
        i += 1
        print("getting results...." + str(i))
        results.append(getAOrgPro(org, start_date, end_date))

    all_output_objects = {"FinancialRatioData": []}
    all_accounts_frame = pd.DataFrame(index=index2, columns=examine_ratios.insert(0, 'OrganisationId'))
    i = 0

    with open('C:\\Temp\\bracketdf\\brackets0_j-1557900677.3785505.json') as json_file:
        brackets_data = json.load(json_file)

    for curr_result in results:
        if curr_result is None:
            continue

        if len(curr_result) < 2:
            continue

        industry_result = get_org_metrics(curr_result['OrganisationId'])['industryCodeId']
        # get_industry_name(industry_result, 0)

        i += 1
        print("calculating ratios...." + str(i))

        base_ratios_df = pd.DataFrame(index=index2, columns=examine_ratios)
        # base_ratios_df = base_ratios_df.fillna(0)  # with 0s rather than NaNs

        for v in examine_ratios:
            for index, row in base_ratios_df.iterrows():
                ratio_value = [x['value'] for x in curr_result['all_amounts'] if
                               x['ratio_name'] == v and x['data_date'] == index]
                row[v] = numpy.float64(ratio_value[0]) if ratio_value else numpy.nan

        base_ratios_df['OrganisationId'] = curr_result['OrganisationId']
        # base_ratios_df.to_csv('C:\\Temp\\base_df-' + timestamp_str + '.csv', sep=',', encoding='utf-8')

        output_object = {'OrganisationId': curr_result['OrganisationId'], 'DataDate': end_date,
                         'FeatureData': {'ActivityMetric': {}, 'LiquidityMetric': {}, \
                                         'GrowthMetric': {}, 'LeverageMetric': {}, 'ProfitabilityMetric': {},
                                         'SizeMetric': {}}}

        # with open('C:\\Temp\\braketdf\\ratio_breakpoints.json') as bracket_data_file:
        #    braket_data = json.load(bracket_data_file)

        # activity ratios
        inventory_to_sales = base_ratios_df['Inventory'][0] / sum(base_ratios_df['Net Sales'][0:11])
        change_in_ar_turnover = (base_ratios_df['ASS.CUR.REC.TRA (Closing)'][0] / sum(
            base_ratios_df['Net Sales'][0:11])) - \
                                (base_ratios_df['ASS.CUR.REC.TRA (Closing)'][12] / sum(
                                    base_ratios_df['Net Sales'][12:23]))
        current_liabilities_to_sales = base_ratios_df['Current Liabilities'][0] / sum(base_ratios_df['Net Sales'][0:11])
        # normalised_inventory_to_sales = braket_data['ratio_breakpoints']
        output_object['FeatureData']['ActivityMetric'] = {
            "InventoryToSales": {"RawValue": inventory_to_sales, "NormValue": ""}, \
            "ChangeInARTurnover": {"RawValue": change_in_ar_turnover, "NormValue": ""}, \
            "CurrentLiabilitiesToSales": {"RawValue": current_liabilities_to_sales, "NormValue": ""}}

        # growth ratios
        sales_growth = (sum(base_ratios_df['Net Sales'][0:11] / sum(base_ratios_df['Net Sales'][12:23]))) - 1
        output_object['FeatureData']['GrowthMetric'] = {"SalesGrowth": {"RawValue": sales_growth, "NormValue": ""}}

        # leverage ratios
        retained_earnings_to_current_liabilites = base_ratios_df['Retained Earnings'][0] / \
                                                  base_ratios_df['Current Liabilities'][0]
        leverage_ratio = base_ratios_df['ACCTTYPE/NONCURRENT (Closing)'][0] / (
                    base_ratios_df['ACCTTYPE/NONCURRENT (Closing)'][0] + base_ratios_df['Tangible Net Worth'][0])
        output_object['FeatureData']['LeverageMetric'] = {
            "RetainedEarningsToCurrentLiabilities": {"RawValue": retained_earnings_to_current_liabilites,
                                                     "NormValue": ""}, \
            "LeverageRatio": {"RawValue": leverage_ratio, "NormValue": ""}}
        # liquidity ratios
        cash_and_securities_to_assets = base_ratios_df['ACCTTYPE/BANK (Closing)'][0] / base_ratios_df['Total Assets'][0]
        output_object['FeatureData']['LiquidityMetric'] = {
            "CashAndSecuritiesToAssets": {"RawValue": cash_and_securities_to_assets, "NormValue": ""}}

        # profitability ratios
        return_on_assets = sum(base_ratios_df['Net Income'][0:11]) / base_ratios_df['Total Assets'][0]
        change_in_return_on_assets = (sum(base_ratios_df['Net Income'][0:11]) / base_ratios_df['Total Assets'][0]) - \
                                     (sum(base_ratios_df['Net Income'][12:23]) / base_ratios_df['Total Assets'][12])
        _cash_flow = sum(base_ratios_df['Earnings Before Interest and Tax (EBIT)'][0:11]) + \
                     (base_ratios_df['LIA.CUR.PAY.TRA (Closing)'][0] - base_ratios_df['LIA.CUR.PAY.TRA (Closing)'][
                         12]) - \
                     (base_ratios_df['ASS.CUR.REC.TRA (Closing)'][0] - base_ratios_df['ASS.CUR.REC.TRA (Closing)'][
                         12]) - \
                     (base_ratios_df['Inventory'][0] - base_ratios_df['Inventory'][12])

        cash_flow_to_interest_expense = _cash_flow / sum(base_ratios_df['Interest Expense'][0:11])

        output_object['FeatureData']['ProfitabilityMetric'] = {
            "ReturnOnAssets": {"RawValue": return_on_assets, "NormValue": ""}, \
            "ChangeInReturnOnAssets": {"RawValue": change_in_return_on_assets, "NormValue": ""}, \
            "CashFlowToInterestExpense": {"RawValue": cash_flow_to_interest_expense, "NormValue": ""}}
        # size ratios
        real_total_assets = base_ratios_df['Total Assets'][0]
        output_object['FeatureData']['SizeMetric'] = {
            "RealTotalAssets": {"RawValue": real_total_assets, "NormValue": ""}}

        all_output_objects['FinancialRatioData'].append(output_object)
        all_accounts_frame = all_accounts_frame.append(base_ratios_df)

    with open('C:\\Temp\\distr_data\\results0_j-' + timestamp_str + '.json', 'w') as fp:
        json.dump(all_output_objects, fp, indent=4, sort_keys=False)

    all_accounts_frame.to_csv('C:\\Temp\\distr_data\\base0-' + timestamp_str + '.csv', sep=',', encoding='utf-8')

    test_breakpoint = 0


def calcuateDistributios():
    RatioLoc = [{"RatioTree": "ActivityMetric", "RatioName": "InventoryToSales"},
                {"RatioTree": "ActivityMetric", "RatioName": "ChangeInARTurnover"},
                {"RatioTree": "ActivityMetric", "RatioName": "CurrentLiabilitiesToSales"}, \
                {"RatioTree": "GrowthMetric", "RatioName": "SalesGrowth"},
                {"RatioTree": "LeverageMetric", "RatioName": "RetainedEarningsToCurrentLiabilities"},
                {"RatioTree": "LeverageMetric", "RatioName": "LeverageRatio"}, \
                {"RatioTree": "LiquidityMetric", "RatioName": "CashAndSecuritiesToAssets"},
                {"RatioTree": "ProfitabilityMetric", "RatioName": "ReturnOnAssets"},
                {"RatioTree": "ProfitabilityMetric", "RatioName": "ChangeInReturnOnAssets"}, \
                {"RatioTree": "ProfitabilityMetric", "RatioName": "CashFlowToInterestExpense"},
                {"RatioTree": "SizeMetric", "RatioName": "RealTotalAssets"}]

    timestamp_str = str(time.time())

    RawList = {}
    OutputObj = {}

    for theRatio in RatioLoc:
        RawList[theRatio["RatioName"]] = []

    file_path = 'C:\\Temp\\distr_data\\'
    for filename in os.listdir(file_path):
        if filename.endswith(".json"):
            with open(file_path + filename) as json_file:
                ratio_data = json.load(json_file)

                for orgRatios in ratio_data['FinancialRatioData']:
                    for theRatio in RatioLoc:
                        RawList[theRatio["RatioName"]].append(
                            orgRatios['FeatureData'][theRatio["RatioTree"]][theRatio["RatioName"]]['RawValue'])

    for theRatio in RatioLoc:
        curr_list = RawList[theRatio["RatioName"]]
        number_of_entries = len(curr_list)
        number_of_zeroes = len([x for x in curr_list if x == 0])
        number_of_nan = len([x for x in curr_list if numpy.isnan(x)])
        number_of_infinities = len([x for x in curr_list if numpy.isinf(x)])

        nonzero_array = [x for x in curr_list if ~numpy.isinf(x) and ~numpy.isnan(x) and x != 0]
        nonzero_array = sorted(nonzero_array)
        number_of_nonzeroes = len(nonzero_array)

        pos = 0
        nxtbpt = 1
        breakpoint_array = []
        for v in nonzero_array:
            current_pct = 100 * float(pos) / float(number_of_nonzeroes)
            if current_pct >= float(nxtbpt):
                breakpoint_array.append({"percentile": nxtbpt, "metric_value:": v})
                nxtbpt += 1
            pos += 1

        OutputObj[theRatio["RatioName"]] = {"number_of_entries": number_of_entries,
                                            "number_of_zeroes": number_of_zeroes,
                                            "number_of_infinities": number_of_infinities, \
                                            "number_of_nan": number_of_nan, "number_of_nonzeroes": number_of_nonzeroes,
                                            "metric_breakpoints": breakpoint_array}

    with open('C:\\Temp\\brackets0_j-' + timestamp_str + '.json', 'w') as fp:
        json.dump(OutputObj, fp, indent=4, sort_keys=False)

    breakpo4 = 0


def main():
    # orgs = [ 'AC9478E7-2E5C-4940-96F4-94D3CCD5F3D5']
    # org_Results = get_org_metrics(orgs[0])

    # calcuateDistributios()

    for i in range(0, 1):
        calculateScores()

    # org = 'B90C47DD-FAD1-489D-8153-550DF416E26B'
    # orgstuff = get_org_metrics(org)['industryCodeId']

    k = 4


if __name__ == '__main__':
    main()
