import requests
import time
import pandas as pd
import json
import io
import psycopg2
import paypalrestsdk
from paypalrestsdk import Payment
import smtplib
# Here are the email package modules we'll need
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os import listdir
from os.path import isfile, join
import base64
from requests.auth import HTTPBasicAuth
import fnmatch
import random
import datetime

townsie_app_un = 'djrp'
townsie_app_pw = 'BOUT e2TP wcjt qgDo ZxPf Pvxm'

townsie_wc_un = 'ck_89d3ec745ea0f04fa5de55ee4b5e814e5dd4c6f8'
townsie_wc_pw = 'cs_7dcf67e4372bc546bc5ddb19fa82b9eb5e6caa3f'

def send_dev_mail(to, subject, body):
    gmail_user = 'dev@townsie.com'
    gmail_password = 'Oq8r8ZZfFb!'

    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(gmail_user, gmail_password)

    mytext = body
    msg = MIMEText(mytext)
    msg["From"] = gmail_user
    msg["To"] = to
    msg["Subject"] = subject

    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(gmail_user, gmail_password)
    server.sendmail(gmail_user, to, msg.as_string())
    server.close()

def try_get_wc_data():
    r = requests.get('https://townsie.com/wp-json/wc/v3/orders', auth=HTTPBasicAuth(townsie_wc_un, townsie_wc_pw))
    r_text = r.text
    r_json = json.loads(r_text)

    brk = 0;

    return;

def try_get_hlf_data():
    headers = {
        'key' : 'Content-Type',
        'name': 'Content-Type',
        'value': 'application/x-www-form-urlencoded',
        'type': 'text',
    }
    body = {
        "mode": "urlencoded",
        "urlencoded": []
    }
    url = 'http://3.14.161.62:6000/api/read-all'
    r = requests.get(url, headers=headers, data=body)
    r_text = r.text
    r_json = json.loads(r_text)

    brk = 0;

    return;


def setup_caps_server():
    info = {}
    headers = {
        'key': 'Content-Type',
        'name': 'Content-Type',
        'value': 'application/json',
        'type': 'text',
    }
    body = {
        "password": "sdjfEEmfu!82xm"
    }
    url = 'http://3.14.161.62:6000/api/reset-server'
    r = requests.post(url, headers=headers, data=body, auth=HTTPBasicAuth('admin', 'sdjfEEmfu!82xm'))
    r_text = r.text
    r_json = json.loads(r_text)
    log_db('log_reset_server', [{"new_inst_id": "------"}])
    time.sleep(5)
    brk = 0

    headers = {
        'key': 'Content-Type',
        'name': 'Content-Type',
        'value': 'application/json',
        'type': 'text',
    }
    body = {
        "town": "demo"
    }
    url = 'http://3.14.161.62:6000/api/create-town'
    r = requests.post(url, headers=headers, data=body, auth=HTTPBasicAuth('admin', 'sdjfEEmfu!82xm'))
    r_text = r.text
    r_json = json.loads(r_text)
    info['town_id'] = r_json['message']
    log_db('log_ref_objects', [{"obj_type": "town", "obj_value": "demo", "obj_id": r_json['message']}])
    brk = 0

    headers = {
        'key': 'Content-Type',
        'name': 'Content-Type',
        'value': 'application/json',
        'type': 'text',
    }
    body = {
        "shape": "local_cap"
    }
    url = 'http://3.14.161.62:6000/api/create-shape'
    r = requests.post(url, headers=headers, data=body, auth=HTTPBasicAuth('admin', 'sdjfEEmfu!82xm'))
    r_text = r.text
    r_json = json.loads(r_text)
    info['local_cap'] = r_json['message']
    log_db('log_ref_objects', [{"obj_type": "shape", "obj_value": "local_cap", "obj_id": r_json['message']}])
    brk = 0

    headers = {
        'key': 'Content-Type',
        'name': 'Content-Type',
        'value': 'application/json',
        'type': 'text',
    }
    body = {
        "shape": "community_cap"
    }
    url = 'http://3.14.161.62:6000/api/create-shape'
    r = requests.post(url, headers=headers, data=body, auth=HTTPBasicAuth('admin', 'sdjfEEmfu!82xm'))
    r_text = r.text
    r_json = json.loads(r_text)
    info['community_cap'] = r_json['message']
    log_db('log_ref_objects', [{"obj_type":"shape","obj_value":"community_cap","obj_id":r_json['message']}])
    brk = 0

    return info

def read_cap_balances():
    url = 'http://3.14.161.62:6000/api/read-all'
    r = requests.get(url, verify=False, auth=HTTPBasicAuth('admin', 'sdjfEEmfu!82xm'))
    r_text = r.text
    r_json = json.loads(r_text)
    brk = 0

def initialise_users_with_caps(info, users):
    i = 0
    headers = {
        'key': 'Content-Type',
        'name': 'Content-Type',
        'value': 'application/x-www-form-urlencoded',
        'type': 'text',
    }

    for user in users:
        if 'wc_product_vendors_admin_vendor' in user['user_role']:
            role = 'shopkeeper'
            cap_color = 'blue'
            cap_size = '5000'
            cap_shape = info['local_cap']

        else:
            role = 'local'
            cap_color = 'blue'
            cap_size = '20'
            cap_shape = info['community_cap']

        body = {
            'cap_owner':user['user_email'],
            'userwcid':user['ID'],
            'password':'password',
            'usertype':role
        }

        url = 'http://3.14.161.62:6000/api/create-owner'
        r = requests.post(url, headers=headers, data=body, auth=HTTPBasicAuth('admin', 'sdjfEEmfu!82xm'))
        r_text = r.text
        r_json = json.loads(r_text)
        owner_id = r_json["message"]

        body = {
            'color': cap_color,
            'size': cap_size,
            'shape_id': cap_shape,
            'owner_id': [owner_id],
            'town_id': info['town_id']
        }

        url = 'http://3.14.161.62:6000/api/create-cap'
        r = requests.post(url, headers=headers, data=body, auth=HTTPBasicAuth('admin', 'sdjfEEmfu!82xm'))
        r_text = r.text
        r_json = json.loads(r_text)

        i += 1

    brk = 0

    return;

def identify_test_wp_users():

    townsie_app_un = 'djrp'
    townsie_app_pw = 'BOUT e2TP wcjt qgDo ZxPf Pvxm'

    credentials = "%s:%s" % (townsie_app_un, townsie_app_pw)
    encode_credential = base64.b64encode(credentials.encode('utf-8')).decode('utf-8').replace("\n", "")

    headers = {
        "Authorization": ("Basic %s" % encode_credential)
    }

    i = 1;
    lastPage = False

    normie_ids = [];
    shopkeep_ids = [];
    users = [];

    while not lastPage:
        url = 'http://townsie.com/wp-json/wp/v2/usersExtra?page='+str(i)
        r = requests.post(url, headers=headers)
        r_text = r.text
        r_json = json.loads(r_text)

        for u_json in r_json:
            if 'wc_product_vendors_admin_vendor' in u_json['user_role']:
                if len(u_json['shopkeeper_ids']) > 0:
                    shopkeep_ids.append(u_json['ID']);
            elif 'customer' in u_json['user_role']:
                normie_ids.append(u_json['ID']);

        i += 1

        print('page ' + str(i) + ' done...')
        if len(r_json) < 10:
            lastPage = True

    data_ids = shopkeep_ids + random.sample(normie_ids, 5)

    for id in data_ids:
        url = 'http://townsie.com/wp-json/wp/v2/usersExtra/' + id
        r = requests.post(url, headers=headers)
        r_text = r.text
        r_json = json.loads(r_text)
        users.append(r_json[0])


    return users


def testdatabase():
    print("hello world")
    conn = psycopg2.connect(dbname='testdatabase', user='postgres', password='irzyebee', host='localhost', \
                            port='5432', sslmode='prefer')
    cur = conn.cursor()
    cur.execute('SELECT testcolumn FROM public.testtable;')
    my_value = cur.fetchone()
    print(my_value)
    print("all done")

def log_db(tname, items):
    conn = psycopg2.connect(dbname='marketcap', user='postgres', password='irzyebee', host='localhost', \
                            port='5432', sslmode='prefer')
    for item in items:
        colstr = "time_ticks,time_stamp"
        valstr = str(int(time.time())) + ",'" + str(datetime.datetime.now()) + "'"
        for key, value in item.items():
            colstr += "," + key
            valstr += "," + "'" + value + "'"

        sqlquery = 'INSERT INTO ' + tname + '(' + colstr + ') VALUES (' + valstr + ')'
        cur = conn.cursor()
        cur.execute(sqlquery)
        conn.commit()
        cur.close()

def try_get_wp_data():

    townsie_app_un = 'djrp'
    townsie_app_pw = 'BOUT e2TP wcjt qgDo ZxPf Pvxm'

    credentials = "%s:%s" % (townsie_app_un, townsie_app_pw)
    encode_credential = base64.b64encode(credentials.encode('utf-8')).decode('utf-8').replace("\n", "")

    headers = {
        "Authorization": ("Basic %s" % encode_credential)
    }

    url = 'http://townsie.com/wp-json/wp/v2/users?per_page=100'
    r = requests.post(url, headers=headers)
    r_text = r.text
    r_json = json.loads(r_text)

    url = 'http://townsie.com/wp-json/wp/v2/usersExtra?page=20'
    r = requests.post(url, headers=headers)
    r_text = r.text
    r_json = json.loads(r_text)

    url = 'http://townsie.com/wp-json/wp/v2/users/874'
    r = requests.post(url, headers=headers)
    r_text = r.text
    r_json = json.loads(r_text)

    url = 'https://townsie.com/wp-json/wp/v2/localListings/'
    r = requests.post(url, headers=headers)
    r_text = r.text
    r_json = json.loads(r_text)

    brk = 0;

    return;

def main():

    testdatabase()

    print("resetting caps server")
    info = setup_caps_server()

    print("identifying test users")
    users = identify_test_wp_users()

    print("creating new users")
    initialise_users_with_caps(info, users)

    print("main done")

if __name__ == '__main__':
    main()