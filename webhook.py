
from flask import Flask, request, abort

app = Flask(__name__)

@app.route('/webhook', methods=['POST'])
def webhook():
    if request.method == 'POST':
        #print(request.json)
        print("hi mom!")
        return 'hallo wald-44', 200
    elif request.method == 'GET':
        return 'hallo wald-44', 200
    else:
        abort(400)

@app.route('/process', methods=['POST', 'GET'])
def process():
    if request.method == 'POST':
        #print(request.json)
        print("process")
        print(request)
        return 'SUCCESS POST', 200
    elif request.method == 'GET':
        tempDonald = request.args
        print(tempDonald)
        return 'SUCCESS GET', 200
    else:
        abort(400)

@app.route('/cancel', methods=['POST'])
def cancel():
    if request.method == 'POST':
        #print(request.json)
        print("cancel")
        print(request)
        return 'SUCCESS POST', 200
    elif request.method == 'GET':
        return 'SUCCESS GET', 200
    else:
        abort(400)

def testdata():
    #curl -H "Content-Type: application/json" -X POST -d "{'data': 'This is some test data'}" 127.0.0.1:5000/webhook
    return 0

if __name__ == '__main__':
    app.debug = True
    #app.run(host='0.0.0.0', port=5000)
    app.run()