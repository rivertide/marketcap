import psycopg2
from psycopg2.extras import NamedTupleCursor
import config
import hashlib
import random
import time

# Common function to provide db connection
def getDb():
    return psycopg2.connect(dbname=config.db_name, user=config.db_user, password=config.db_pass, host=config.db_host, \
                                port=config.db_port, sslmode='prefer', cursor_factory = NamedTupleCursor)


def createUniqueID(sellerID, buyerID):
    timestamp = time.time()
    randomID = random.random()*999999+timestamp
    uid = str(randomID) + " " + str(timestamp) + " " + str(sellerID) + " " + str(buyerID)    
    return hashlib.md5(uid.encode()).hexdigest()

# Create db tables on app startup
# Just for development only. For production, should be managed separately.
def setup_db_tables(): 
    commands = (
        """
        CREATE TABLE coin_transfer (
            id SERIAL PRIMARY KEY, 
            transaction_id VARCHAR (50) NOT NULL, 
            seller_id VARCHAR (40) NOT NULL, 
            buyer_id VARCHAR (40) NOT NULL, 
            payment_amount FLOAT NULL,
            fee_amount FLOAT NULL,
            cap_amount FLOAT NOT NULL,
            cap_shape VARCHAR (30) NOT NULL,
            cap_color VARCHAR (30) NOT NULL,
            status VARCHAR (30) NOT NULL,
            transaction_type VARCHAR (30) NOT NULL,
            payment_uniq_id VARCHAR (50) NULL, 
            fee_uniq_id VARCHAR (50) NULL, 
            wc_order_id VARCHAR (50) NULL, 
            wc_product_id VARCHAR (50) NULL, 
            user_meta_data VARCHAR(300) NULL, 
            timestamp_utc TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        )
        """
        )
    #run_structure_query(commands)

# Drop all old tables  
def drop_tables():
    commands = [
        "DROP TABLE IF EXISTS coin_transfer",
    ]
    #run_structure_query(commands)

# Common function to run the db structural queries
def run_structure_query(commands):
    try:
        conn = getDb()
        cur = conn.cursor()
        # create table one by one
        for command in commands:
            cur.execute(command)
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

