import falcon
from wsgiref import simple_server
import os

import AppHelper    
from controllers.PaidCapsTransfer import PaidCapsTransfer
from controllers.SimpleCapsTransfer import SimpleCapsTransfer
from controllers.CapsTransferViaOrder import CapsTransferViaOrder
from controllers.GetUserCapBalance import GetUserCapBalance
from controllers.GetCapTypes import GetCapTypes
import controllers.PaypalController as PaypalController

# Flacon api instance
api = falcon.API()

# Falcon api routes
api.add_route('/paid-caps-transfer', PaidCapsTransfer())
api.add_route('/simple-caps-transfer', SimpleCapsTransfer())
api.add_route('/order-caps-transfer', CapsTransferViaOrder())
api.add_route('/get-cap-types', GetCapTypes())
api.add_route('/get-user-cap-balance/{id}', GetUserCapBalance())
api.add_route('/request-payment/{id}', PaypalController.ActionStartPayment())
api.add_route('/payments/create-order/{id}', PaypalController.ActionCreatePayment())
api.add_route('/payments/capture-payment/', PaypalController.ActionCapturePayment())
api.add_route('/payment-success', PaypalController.ActionPaymentSuccess())
api.add_route('/webhook/paypal-ipn', PaypalController.ActionPaypalIpn())

if __name__ == '__main__':
    ##################################
    # for development purpose only
    # delete all old tables if they already exists
    #   AppHelper.drop_tables()
    # create fresh tables
    #   AppHelper.setup_db_tables()
    ###################################
    # init server
    with simple_server.make_server('', os.getenv('PORT', 5000), api) as httpd:
        httpd.serve_forever()