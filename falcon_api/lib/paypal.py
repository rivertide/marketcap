import requests
import urllib.parse
import json
import base64
import config

def CreateCheckout(params):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept-Language': 'en_US'
    }
    paymentParam = getPaymentParams(params)
    paypal_creds = get_paypal_creds()
    reqParams = {
        "USER": paypal_creds["user"], # your paypal business account email id
        "PWD": paypal_creds["password"], # your paypal password
        "SIGNATURE": paypal_creds["signature"], # Your account signature
        "METHOD":"SetExpressCheckout",
        "VERSION":124.0,
        "RETURNURL":"https://example.com/return", # we don't use this. but leave it as it is
        "CANCELURL":"https://example.com/cancel", # we don't use this. but leave it as it is
        
    }
    
    body = {**reqParams, **paymentParam}
    print("\n ######### \n checkout params \n", body , "\n ######### \n")
    url = get_api_url()
    response = requests.post(url, headers=headers, data=body)
    
    decodedString = urllib.parse.parse_qs(response.text)
    response = sanitizeResponse(decodedString)
    if response['TOKEN'] :
        token = response['TOKEN']
    else:
        token = ''
    print(token)
    return token

def getCheckoutDetail(token):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept-Language': 'en_US'
    }
    paypal_creds = get_paypal_creds()
    reqParams = {
        "USER": paypal_creds["user"], # your paypal business account email id
        "PWD": paypal_creds["password"], # your paypal account password
        "SIGNATURE": paypal_creds["signature"], # Your account signature
        "METHOD" : "GetExpressCheckoutDetails",
        "TOKEN" : token,
        "VERSION" : "124.0"
    }

    body = reqParams
    
    url = get_api_url()
    response = requests.post(url, headers=headers, data=body)
    print("\n\n ####### text form \n\n",response.text,"\n\n ########## \n\n")
    decodedString = urllib.parse.parse_qs(response.text)    
    return sanitizeResponse(decodedString)


def doExpressCheckoutPayment(token, payerID):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept-Language': 'en_US'
    }
    checkoutDetail = getCheckoutDetail(token)
    paypal_creds = get_paypal_creds()
    reqParams = {
        "USER": paypal_creds["user"], # your paypal business account email id
        "PWD": paypal_creds["password"], # your paypal account password
        "SIGNATURE": paypal_creds["signature"], # Your account signature
        "METHOD":"DoExpressCheckoutPayment",
        "TOKEN":token,
        "PAYERID":payerID,
        "VERSION":93.0,
    }
    
    body = { **reqParams, **checkoutDetail}

    print("\n\n ********* \n\n Capture body \n \n ", body ,"\n ********** \n ")
    
    url = get_api_url()
    
    response = requests.post(url, headers=headers, data=body)
    
    decodedString = urllib.parse.parse_qs(response.text)    
    print("\n ************ \n Payment capture response \n \n",decodedString, "\n ************* \n ")
    return sanitizeResponse(decodedString)

def getPaymentParams(params):
    paypal_creds = get_paypal_creds()
    paymentParams = {
        "PAYMENTREQUEST_0_PAYMENTACTION":"Sale",
        "PAYMENTREQUEST_0_AMT": params['paymentAmount'],
        "PAYMENTREQUEST_0_CURRENCYCODE": params['currencyCode'],
        "PAYMENTREQUEST_0_DESC":"Seller Payment",
        "PAYMENTREQUEST_0_PAYMENTREQUESTID" : params['paymentUniqueId'], # unique random id for user 1 payment
        "PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID": params['sellerEmail'], #user 1 paypal email
        "PAYMENTREQUEST_1_PAYMENTACTION":"Sale",
        "PAYMENTREQUEST_1_AMT": params['feeAmount'],
        "PAYMENTREQUEST_1_CURRENCYCODE" : params['currencyCode'],
        "PAYMENTREQUEST_1_DESC":"Admin fee",
        "PAYMENTREQUEST_1_PAYMENTREQUESTID" : params['feeUniqueId'], # unique random id for user 1 payment
        "PAYMENTREQUEST_1_SELLERPAYPALACCOUNTID" : paypal_creds['admin_email'] #user 2 paypal email
    }
    return paymentParams


def sanitizeResponse(response):
    filteredData = {}
    for (key, val) in response.items():
        filteredData[key] = val[0]
    
    return filteredData

def get_api_url():
    if config.paypal_env == 'sandbox':
        url = 'https://api-3t.sandbox.paypal.com/nvp'
    else:
        url = 'https://api-3t.paypal.com/nvp'
    return url

def get_paypal_creds():
    if config.paypal_env == 'sandbox':
        creds = config.paypal_sandbox
    else:
        creds = config.paypal_live
    return creds