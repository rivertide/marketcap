import config
import AppHelper
import hashlib
import random
import time

# valid status strings for payment 
STATUS_CREATED = 'CREATED'
STATUS_PAID = 'PAID'
STATUS_CANCELLED = 'CANCELLED'



# Intialize new payment entry in database
def CreatePayment(sellerID, buyerID, paymentAmount, feeAmount):
    conn = AppHelper.getDb()
    cur = conn.cursor()
    #create payment ipn_custom, payment unique id
    paymentUniqueId = createUniqueID(sellerID , buyerID)
    feeUniqueId = createUniqueID('operator' , buyerID)
    paymentStatus = STATUS_CREATED
    totalPayment = paymentAmount + feeAmount 
    command = 'INSERT INTO payments (amount, user_amount, operator_fee, payment_unique_id, fee_unique_id, status) VALUES(%s,%s,%s,%s,%s,%s) RETURNING id'
    cur.execute(command, (totalPayment, paymentAmount, feeAmount, paymentUniqueId, feeUniqueId, paymentStatus))
    conn.commit()
    paymentId = cur.fetchone()[0]
    cur.close()
    return { "id" : paymentId, "uniqueId" : paymentUniqueId} 

# local function to create unique custom id for paypal
# This will to recoganize the payment in paypal IPN webhook    
def createUniqueID(sellerID, buyerID):
    timestamp = time.time()
    randomID = random.random()*999999+timestamp
    uid = str(randomID) + " " + str(timestamp) + " " + str(sellerID) + " " + str(buyerID)    
    return hashlib.md5(uid.encode()).hexdigest()
    
# get transaction detail
def GetPayment(transaction_id):
    conn = AppHelper.getDb()
    cur = conn.cursor()
    command = 'SELECT * FROM coin_transfer WHERE transaction_id = %s'
    cur.execute(command, (transaction_id,))
    return cur.fetchone()


# mark paypal payment status complete
def MarkPaymentComplete(paymentUniqueId):
    conn = AppHelper.getDb()
    cur = conn.cursor()
    command = 'UPDATE coin_transfer SET status = %s WHERE payment_uniq_id = %s'
    cur.execute(command, ('PAYPAL_PAYMENT_MADE', paymentUniqueId))
    conn.commit()

# mark overall transaction status complete
def MarkTransactionComplete(paymentUniqueId):
    conn = AppHelper.getDb()
    cur = conn.cursor()
    command = 'UPDATE coin_transfer SET status = %s WHERE payment_uniq_id = %s'
    cur.execute(command, ('CAP_TRANSFER_MADE', paymentUniqueId))
    conn.commit()
