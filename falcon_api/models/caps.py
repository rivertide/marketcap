import config
import AppHelper
import requests
import json
import base64
from requests.auth import HTTPBasicAuth
from models.email import SendEmail

def TransferCapsForPayment(paymentUniqueId):

    conn = AppHelper.getDb()
    cur = conn.cursor()
    command = 'SELECT * FROM coin_transfer WHERE payment_uniq_id = %s'
    cur.execute(command, (paymentUniqueId,))
    transactionDetail =  cur.fetchone()

    user_meta_data = json.loads(transactionDetail.user_meta_data);
    seller = user_meta_data['seller']
    buyer = user_meta_data['buyer']

    seller['email'] = 'aman2talk+seller@gmail.com'    
    buyer['email'] = 'aman2talk+buyer@gmail.com'    

    fromUserId = transactionDetail.seller_id
    toUserId = transactionDetail.buyer_id
    cap_color = transactionDetail.cap_color
    cap_shape = transactionDetail.cap_shape
    cap_amount = transactionDetail.cap_amount

    transferResponse = transfer_coins_between_users(fromUserId, toUserId, cap_color, cap_shape, cap_amount)
    if transferResponse['response_code'] == 200:
        # send success notification to buyer
        subject = 'The Caps transfer has gone through'
        total_amount = transactionDetail.payment_amount + transactionDetail.fee_amount
        data = {
            "USER_NAME" : seller['name'],
            "RECEIVED_TITLE" : "<strong>Shape:</strong>"+cap_shape+",<strong> Color:</strong>"+cap_color,
            "RECEIVED_AMOUNT" : cap_amount,
            "PAID_TITLE" : "Total Payment",
            "PAID_AMOUNT" : '$'+ str(total_amount),
            "POSTGRE_TRAN_ID" : transactionDetail.transaction_id
        }
        SendEmail(template = 'pct/pct_caps_transfer_success', to = buyer['email'], subject = subject, data = data)

        # send success notification to seller
        data = {
            "USER_NAME" : buyer['name'],
            "RECEIVED_TITLE" : "Total Payment",
            "RECEIVED_AMOUNT" : '$'+ str(transactionDetail.payment_amount),
            "PAID_TITLE" : "<strong>Shape:</strong>"+cap_shape+",<strong> Color:</strong>"+cap_color,
            "PAID_AMOUNT" : cap_amount,
            "POSTGRE_TRAN_ID" : transactionDetail.transaction_id
        }
        SendEmail(template = 'pct/pct_caps_transfer_success', to = seller['email'], subject = subject, data = data)
    else:
        subject = 'The Caps transfer failed'
        data = {
            "POSTGRE_TRAN_ID" : transactionDetail.transaction_id
        }
        SendEmail(template = 'pct/pct_caps_transfer_failed', to = buyer['email'], subject = subject, data = data)
    

def transfer_coins_between_users(fromUserId, toUserId, color, shape, amount):
    amount = int(amount)
    url = config.api_host + '/api/read-owner/' + toUserId
    r = requests.get(url, verify=False, auth=HTTPBasicAuth(config.api_username, config.api_password))
    r_text = r.text
    r_json = json.loads(r_text)

    if 'success' not in r_json or r_json['success'] != True:
        return {"response_code": 400, "response_text": "Problem reading user recieving coin"}
    print("something log 1", r_json)
    url = config.api_host + '/api/read-owner/' + fromUserId
    r = requests.get(url, verify=False, auth=HTTPBasicAuth(config.api_username, config.api_password))
    r_text = r.text
    r_json = json.loads(r_text)

    if 'success' not in r_json or r_json['success'] != True:
        return { "response_code":400, "response_text": "Problem reading user providing coin" }
    print("something log 2", r_json)
    color_size_array = [x for x in r_json["message"]["caps"] if x["color"] == color and x["shape"]["name"] == shape]
    color_size_array = sorted(color_size_array, key=lambda k: k['size'])

    total_color_size = sum([x['size'] for x in color_size_array])

    if total_color_size < amount:
        return {"response_code": 400, "response_text": "User did not have enough caps of right color and shape"}

    rt_amnt_rem = amount
    rt_xfer_caps = []
    for c in color_size_array:
        if rt_amnt_rem <= 0:
            break;

        if rt_amnt_rem >= c['size']:
            rt_xfer_caps.append(c['id'])
            rt_amnt_rem -= c['size']

        if rt_amnt_rem < c['size']:
            split_amnt = rt_amnt_rem
            body = {
                "cap_id": c["id"],
                "split_size": split_amnt
            }

            url = config.api_host + '/api/split-cap'
            r = requests.post(url, verify=False, auth=HTTPBasicAuth(config.api_username, config.api_password), data=body)
            r_text = r.text
            r_json = json.loads(r_text)

            rt_xfer_caps.append(r_json['message'])
            rt_amnt_rem -= split_amnt

    for t in rt_xfer_caps:
        body = {
            "cap_id": t,
            "owner_id": toUserId
        }

        url = config.api_host + '/api/transfer-cap'
        r = requests.post(url, verify=False, auth=HTTPBasicAuth(config.api_username, config.api_password), data=body)
        r_text = r.text
        r_json = json.loads(r_text)

    return {"response_code": 200, "response_text": r_json}


def check_if_user_has_caps(user_id, color, shape, amount):
    amount = int(amount)
    # api call to chaincode api
    url = config.api_host + "/api/read-owner/" + user_id
    payload = ""
    headers = {
        'Content-Type': "application/json",
    }
    response = requests.request("GET", url, data=payload, headers=headers, auth=HTTPBasicAuth(config.api_username, config.api_password) )
    apiResponse = json.loads(response.text)
    print(apiResponse)
    if (apiResponse["success"] == True and apiResponse["message"]["caps"]): 
        
        color_size_array = [x for x in apiResponse["message"]["caps"] if x["color"] == color and x["shape"]["name"] == shape]
        color_size_array = sorted(color_size_array, key=lambda k: k['size'])

        total_color_size = sum([x['size'] for x in color_size_array])

        if total_color_size < amount:        
            return False
        else:
            return True

    else:
        return -1
