import config
from email.mime.text import MIMEText
import smtplib
from string import Template
import codecs

def SendEmail(to, subject, template, data):
    
    TEMPLATE_PATH = "./templates/email/" + template + '.html' 
    file = codecs.open(TEMPLATE_PATH,"rt")
    htmlContent = file.read()
    parsedContent = Template(htmlContent).safe_substitute(data)

    msg = MIMEText(parsedContent, "html")
    msg["From"] = config.from_address
    msg["To"] = to
    msg["Subject"] = subject

    server = smtplib.SMTP_SSL(config.smtp_host, config.smtp_port)
    server.ehlo()
    server.login(config.smtp_username, config.smtp_password)
    server.sendmail(config.from_address, to, msg.as_string())
    server.close()