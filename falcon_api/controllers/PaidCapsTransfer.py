import falcon
import config
import requests
import json
import base64
import AppHelper
from requests.auth import HTTPBasicAuth
from models.email import SendEmail
from models import caps

# Falcon api route class for [POST : /paid-caps-transfer]
class PaidCapsTransfer:

    def on_post(self, req, resp):
        post_data = req.media
        if(post_data == None):
            resp.status = falcon.HTTP_400
            resp.media = {"response_code" : "400", "response_text" : "Post data is missing"}
            return False
        # request submitted data
        cap_seller_id    =  post_data["seller_id"]
        cap_buyer_id     =  post_data["buyer_id"]
        payment_amount   =  post_data["payment"]
        fee_amount       =  post_data["fee"]
        cap_amount       =  post_data["cap_amount"]
        cap_shape        =  post_data["cap_shape"]
        cap_color        =  post_data["cap_color"]
        transaction_type =  'PCT'
        
        total_payment_amount = int(payment_amount) + int(fee_amount)
        url = config.api_host + "/api/read-all"
        payload = ""
        headers = {
            'Content-Type': "application/json",
        }
        response = requests.request("GET", url, data=payload, headers=headers, auth=HTTPBasicAuth(config.api_username, config.api_password) )
        r_json = json.loads(response.text)
        users = r_json['message']['owners']
        caps_object = r_json['message']['caps']

        cap_user_seller = None
        cap_user_buyer = None

        for user in users: 
            if user["id"] == cap_seller_id:
                cap_user_seller = user
            if user["id"] == cap_buyer_id:
                cap_user_buyer = user

        if (cap_user_seller and cap_user_buyer):

            seller_wp_id = cap_user_seller['wordpressID']
            buyer_wp_id = cap_user_buyer['wordpressID']

            # getting email and display name from wordpress for caps users
            credentials = "%s:%s" % (config.townsie_app_un, config.townsie_app_pw)
            encode_credential = base64.b64encode(credentials.encode('utf-8')).decode('utf-8').replace("\n", "")

            headers = {
                "Authorization": ("Basic %s" % encode_credential)
            }
            url = 'http://devtest.townsie.com/wp-json/python-ctrlr/v1/get-user-detail/' + seller_wp_id + '/'+ buyer_wp_id
            r = requests.get(url, verify=False, headers=headers)
            r_text = r.text
            r_json = json.loads(r_text)
            print(r_json);

            seller = r_json['user1']
            buyer = r_json['user2']
            
            
            user_meta_data = json.dumps({"seller" : seller, "buyer" : buyer}) 

            user_has_caps = caps.check_if_user_has_caps(cap_seller_id, cap_color, cap_shape, cap_amount)
            if user_has_caps == -1 :
                # could not connect cap server
                resp.body = '{"response_code": 400, "response_text": "Could not connect caps server to verify if user have enough caps"}'
                resp.status = falcon.HTTP_400
            
            else:
                if user_has_caps == True:
                    # user have enough cpas of right type so start the process
                    
                    #payment = CreatePayment(sellerId, buyerId, paymentAmount, feeAmount)
                    conn = AppHelper.getDb()
                    cur = conn.cursor()
                    transaction_id = AppHelper.createUniqueID(cap_seller_id, cap_buyer_id)
                    #create payment ipn_custom, payment unique id
                    payment_unique_id = AppHelper.createUniqueID(cap_seller_id, cap_buyer_id)
                    fee_unique_id = AppHelper.createUniqueID('operator' , cap_buyer_id)
                    status = 'TRANSACTION_PENDING'
                    command = 'INSERT INTO coin_transfer (transaction_id, seller_id, buyer_id, cap_amount, cap_shape, cap_color, status, transaction_type, payment_amount, fee_amount, user_meta_data, payment_uniq_id, fee_uniq_id) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING id'
                    cur.execute(command, ( transaction_id, cap_seller_id, cap_buyer_id, cap_amount, cap_shape, cap_color, status, transaction_type, payment_amount, fee_amount, user_meta_data, payment_unique_id, fee_unique_id))
                    conn.commit()

                    #last inserted id
                    transaction_row_id = cur.fetchone()[0]


                    paypal_payment_link = config.app_host + '/request-payment/' + str(transaction_id) 
                    
                    # Seller user has caps of right type
                    # So sending buyer a email with paypal link
                    subject = 'Request for paid caps transfer'
                    data = {
                        "SELLER_NAME" : seller['name'],
                        "CAP_AMOUNT" : cap_amount,
                        "CAP_SHAPE" : cap_shape,
                        "CAP_COLOR" : cap_color,
                        "PAYMENT_AMOUNT" : '$'+ str(payment_amount),
                        "FEE_AMOUNT" : '$'+ str(fee_amount),
                        "TOTAL_PAYMENT" : '$'+ str(total_payment_amount),
                        "TOTAL_AMOUNT" : '$'+ str(total_payment_amount),
                        "PAYMENT_LINK" : paypal_payment_link
                    }
                    SendEmail(template = 'pct/pct_buyer_paypal_btn', to = buyer['email'], subject = subject, data = data)
                    
                    # notify the seller too, regrading their transaction
                    subject = 'Payment Notification'
                    data = {
                        "BUYER_NAME" : buyer['name'],
                        "PAYMENT_AMOUNT" : '$' + str(payment_amount),
                        "CAP_AMOUNT" : cap_amount,
                        "CAP_SHAPE" : cap_shape,
                        "CAP_COLOR" : cap_color,
                        "POSTGRE_TRAN_ID" : transaction_id
                    }
                    SendEmail(template = 'pct/pct_seller_notify_payment_start', to = seller['email'], subject = subject, data = data)

                    resp.status = falcon.HTTP_200
                    resp.media = {"status" : "200", "response_text" : "Transaction initialized and email with paypal link has been sent to buyer"}
                else:
                    resp.status = falcon.HTTP_400
                    resp.media = {"status" : 'FAILED', "message" : "User doesn't has sufficient caps"}
        else:
            resp.status = falcon.HTTP_400
            resp.media = {"status" : "400", "response_text" : "Could not fetch user detail"}
