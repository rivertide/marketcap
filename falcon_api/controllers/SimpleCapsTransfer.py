import falcon
import config
import requests
import json
import base64
import AppHelper
from requests.auth import HTTPBasicAuth
from models.email import SendEmail
from models import caps

# Falcon api route class for [POST : /simple-caps-transfer]
class SimpleCapsTransfer:

    def on_post(self, req, resp):
        post_data = req.media
        if(post_data == None):
            resp.status = falcon.HTTP_400
            resp.media = {"response_code" : "400", "response_text" : "Post data is missing"}
            return False
        seller_id = post_data["seller_id"] 
        buyer_id = post_data["buyer_id"] 
        cap_color = post_data["cap_color"] 
        cap_amount = int(post_data["cap_amount"]) 
        cap_shape = post_data["cap_shape"]
        transaction_type = 'SCT'
        
        user_has_caps = caps.check_if_user_has_caps(seller_id, cap_color, cap_shape, cap_amount)
        if user_has_caps == -1 :
            # could not connect cap server
            resp.body = '{"response_code": 400, "response_text": "Could not connect caps server to verify if user have enough caps"}'
            resp.status = falcon.HTTP_400

        else : 
            if user_has_caps == True:
                # user have enough cpas of right type so start the process
                
                conn = AppHelper.getDb()
                cur = conn.cursor()
                transaction_id = AppHelper.createUniqueID(seller_id, buyer_id)
                status = 'TRANSACTION_PENDING'
                command = 'INSERT INTO coin_transfer (transaction_id, seller_id, buyer_id, cap_amount, cap_shape, cap_color, status, transaction_type) VALUES(%s,%s,%s,%s,%s,%s,%s,%s) RETURNING id'
                cur.execute(command, ( transaction_id, seller_id, buyer_id, cap_amount, cap_shape, cap_color, status, transaction_type))
                conn.commit()

                #last inserted id
                transaction_row_id = cur.fetchone()[0]

                # fetch user detail from wordpress api
                user_id_1 = '1003'
                user_id_2 = '847'

                url = config.api_host + "/api/read-all"
                payload = ""
                headers = {
                    'Content-Type': "application/json",
                }
                response = requests.request("GET", url, data=payload, headers=headers, auth=HTTPBasicAuth(config.api_username, config.api_password) )
                r_json = json.loads(response.text)
                users = r_json['message']['owners']
                caps_object = r_json['message']['caps']

                cap_user_seller = None
                cap_user_buyer = None

                for user in users: 
                    if user["id"] == seller_id:
                        cap_user_seller = user
                    if user["id"] == buyer_id:
                        cap_user_buyer = user
                
                seller_wp_id = cap_user_seller['wordpressID']
                buyer_wp_id = cap_user_buyer['wordpressID']




                credentials = "%s:%s" % (config.townsie_app_un, config.townsie_app_pw)
                encode_credential = base64.b64encode(credentials.encode('utf-8')).decode('utf-8').replace("\n", "")

                headers = {
                    "Authorization": ("Basic %s" % encode_credential)
                }
                url = 'http://devtest.townsie.com/wp-json/python-ctrlr/v1/get-user-detail/' + seller_wp_id + '/'+ buyer_wp_id
                r = requests.get(url, verify=False, headers=headers)
                r_text = r.text
                r_json = json.loads(r_text)
                print(r_json);

                seller = r_json['user1']
                buyer = r_json['user2']
                
                # informing the both seller & buyer that they are exchanging caps
                
                ## Email to buyer
                subject = 'You are are exchanging caps'
                data = {
                    "USER_NAME" : seller['name'],
                    "CAP_AMOUNT" : cap_amount,
                    "CAP_COLOR" : cap_color,
                    "CAP_SHAPE" : cap_shape,
                    "POSTGRE_TRAN_ID" : transaction_id,
                    "TABLE_HEADING" : "You will get",
                }
                SendEmail(template = 'sct/buyer_seller_notify_payment_start', to = buyer['email'], subject = subject, data = data)
                
                ## Email to seller
                data = {
                    "USER_NAME" : buyer['name'],
                    "CAP_AMOUNT" : cap_amount,
                    "CAP_COLOR" : cap_color,
                    "CAP_SHAPE" : cap_shape,
                    "POSTGRE_TRAN_ID" : transaction_id,
                    "TABLE_HEADING" : "You will provide",
                }
                SendEmail(template = 'sct/buyer_seller_notify_payment_start', to = seller['email'], subject = subject, data = data)
                
                #########################################
                # it's time to transfer the caps between users
                cap_transfer_response = caps.transfer_coins_between_users(seller_id, buyer_id, cap_color, cap_shape, cap_amount)

                if(cap_transfer_response['response_code'] == 200):
                    transfer_status = 'CAP_TRANSFER_MADE'
                    
                    # caps transferred succsessfully so notifying both seller & buyer
                    ## Email to buyer
                    subject = 'Caps transferred succsessfully'
                    data = {
                        "USER_NAME" : seller['name'],
                        "CAP_AMOUNT" : cap_amount,
                        "CAP_COLOR" : cap_color,
                        "CAP_SHAPE" : cap_shape,
                        "POSTGRE_TRAN_ID" : transaction_id,
                        "TABLE_HEADING" : "You Received",
                    }
                    SendEmail(template = 'sct/buyer_seller_notify_caps_transfer_success', to = buyer['email'], subject = subject, data = data)
                    
                    ## Email to seller
                    data = {
                        "USER_NAME" : buyer['name'],
                        "CAP_AMOUNT" : cap_amount,
                        "CAP_COLOR" : cap_color,
                        "CAP_SHAPE" : cap_shape,
                        "POSTGRE_TRAN_ID" : transaction_id,
                        "TABLE_HEADING" : "You Provided",
                    }
                    SendEmail(template = 'sct/buyer_seller_notify_caps_transfer_success', to = seller['email'], subject = subject, data = data)

                else:
                    transfer_status = 'CAP_TRANSFER_FAILED'
                    
                    # caps transfer failed so notifying buyer
                    ## Email to buyer
                    subject = 'Caps transfer failed'
                    data = {
                        "POSTGRE_TRAN_ID" : transaction_id
                    }
                    SendEmail(template = 'sct/buyer_notify_caps_transfer_failed', to = buyer['email'], subject = subject, data = data)


                # update the transaction status 
                command = 'UPDATE coin_transfer SET status=%s WHERE id=%s'
                cur.execute(command, (transfer_status, transaction_row_id))
                conn.commit()

                cur.close()

                if(transfer_status == 'CAP_TRANSFER_MADE'):
                    resp.status = falcon.HTTP_200
                    resp.media = {"response_code" : "200", "response_text": "Caps Transferred Successfully"}
                else:
                    resp.status = falcon.HTTP_400
                    resp.media = {"response_code" : "400", "response_text": cap_transfer_response["response_text"]}
                    
            else:
                # user don't have enough caps of right type so send error
                resp.media = {"response_code": 400, "response_text": "User does not have enough caps of right color and shape"}
                resp.status = falcon.HTTP_400

            