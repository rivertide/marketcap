import falcon
import config
import requests
import json
import base64
import AppHelper
from requests.auth import HTTPBasicAuth

# Falcon api route class for [get : /get-user-cap-balance/:id]
class GetUserCapBalance:

    def on_get(self, req, resp, id):
        url = config.api_host + "/api/read-all"
        payload = ""
        headers = {
            'Content-Type': "application/json",
        }
        response = requests.request("GET", url, data=payload, headers=headers, auth=HTTPBasicAuth(config.api_username, config.api_password) )
        apiResponse = json.loads(response.text)
        print(apiResponse)
        caps = [];
        if (apiResponse["success"] == True and apiResponse["message"]["caps"]): 
            for cap in apiResponse["message"]["caps"]:
                if (cap["owner"]["wordpressID"] == id):
                    already_exist = False
                    exisiting_caps = caps
                    for e_cap in exisiting_caps:
                        if e_cap['color'] == cap['color'] and e_cap['shape'] == cap['shape']['name']:
                            e_cap['size'] += cap['size']
                            already_exist = True
                            break

                    if not already_exist:
                        caps.append({
                            "color" : cap['color'],
                            "shape" : cap['shape']['name'],
                            "size" : cap['size']
                        });

        resp.media = {"caps" : caps};
