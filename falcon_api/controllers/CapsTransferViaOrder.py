import falcon
import config
import requests
import json
import base64
import AppHelper
from requests.auth import HTTPBasicAuth
from models.email import SendEmail

# to be deleted
from models import caps
from models.payment import CreatePayment

# Falcon api route class for [POST : /order-caps-transfer]
class CapsTransferViaOrder:

    def on_post(self, req, resp):
        post_data = req.media
        buyer = post_data["buyer"] 
        seller = post_data["seller"] 
        cap_color = post_data["product"]["cap_color"] 
        cap_amount = int(post_data["product"]["cap_amount"]) 
        cap_shape = post_data["product"]["cap_shape"] 
        transaction_type = 'CTO'
        order_id = post_data["order_id"] 
        product_id = post_data["product"]["id"] 
        
        url = config.api_host + "/api/read-all"
        payload = ""
        headers = {
            'Content-Type': "application/json",
        }
        response = requests.request("GET", url, data=payload, headers=headers, auth=HTTPBasicAuth(config.api_username, config.api_password) )
        r_json = json.loads(response.text)
        users = r_json['message']['owners']
        caps_object = r_json['message']['caps']

        cap_user_seller = None
        cap_user_buyer = None
        for user in users: 
            if user["wordpressID"] == str(seller['id']):
                cap_user_seller = user
            if user["wordpressID"] == str(buyer['id']):
                cap_user_buyer = user
            
        if(cap_user_seller and cap_user_buyer):

            cap_user_seller_id = cap_user_seller['id']
            cap_user_buyer_id = cap_user_buyer['id']
            
            # check if seller user has sufficient caps

            color_shape_size_array = [x for x in caps_object if x["owner"]["id"] == cap_user_seller_id and x["color"] == cap_color and x["shape"]["name"] == cap_shape]
            color_shape_size_array = sorted(color_shape_size_array, key=lambda k: k['size'])

            total_caps = sum([x['size'] for x in color_shape_size_array])
    
            conn = AppHelper.getDb()
            cur = conn.cursor()
            transaction_id = AppHelper.createUniqueID(cap_user_seller_id, cap_user_buyer_id)
            status = 'TRANSACTION_PENDING'
            command = 'INSERT INTO coin_transfer (transaction_id, seller_id, buyer_id, cap_amount, cap_shape, cap_color, status, transaction_type, wc_order_id, wc_product_id) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING id'
            cur.execute(command, ( transaction_id, cap_user_seller_id, cap_user_buyer_id, cap_amount, cap_shape, cap_color, status, transaction_type, order_id, product_id))
            conn.commit()

            #last inserted id
            transaction_row_id = cur.fetchone()[0]

            ## Email to buyer
            subject = 'You are are exchanging caps'
            
            ## Cap transfer start Email to seller
            data = {
                "USER_NAME" : buyer['name'],
                "CAP_AMOUNT" : cap_amount,
                "CAP_COLOR" : cap_color,
                "CAP_SHAPE" : cap_shape,
                "POSTGRE_TRAN_ID" : transaction_id
            }
            SendEmail(template = 'cto/cto_seller_notify_payment_start', to = seller['email'], subject = subject, data = data)            

            if total_caps > cap_amount:
                # user have enough cpas of right type so start the
                # process by creating the transaction entry in database
                
                #########################################
                # it's time to transfer the caps between users
                cap_transfer_response = caps.transfer_coins_between_users(cap_user_seller_id, cap_user_buyer_id, cap_color, cap_shape, cap_amount)

                if(cap_transfer_response['response_code'] == 200):
                    transfer_status = 'CAP_TRANSFER_MADE'

                    subject = 'Caps transferred succsessfully'
                    ## Cap Transfer Success Email to seller
                    data = {
                        "USER_NAME" : buyer['name'],
                        "CAP_AMOUNT" : cap_amount,
                        "CAP_COLOR" : cap_color,
                        "CAP_SHAPE" : cap_shape,
                        "POSTGRE_TRAN_ID" : transaction_id,
                        "TABLE_HEADING" : "You Paid",
                    }
                    SendEmail(template = 'cto/cto_caps_transfer_success', to = seller['email'], subject = subject, data = data)

                    ## Cap Transfer Success Email to buyer
                    data = {
                        "USER_NAME" : seller['name'],
                        "CAP_AMOUNT" : cap_amount,
                        "CAP_COLOR" : cap_color,
                        "CAP_SHAPE" : cap_shape,
                        "POSTGRE_TRAN_ID" : transaction_id,
                        "TABLE_HEADING" : "You Received"
                    }
                    SendEmail(template = 'cto/cto_caps_transfer_success', to = buyer['email'], subject = subject, data = data)
                
                else:
                    ## Cap Transfer Failure 
                    transfer_status = 'CAP_TRANSFER_FAILED'
                    
                    subject = 'Caps transfer failed'
                    
                    # Email to seller
                    data = {
                        "POSTGRE_TRAN_ID" : transaction_id
                    }
                    SendEmail(template = 'cto/cto_caps_transfer_failed', to = seller['email'], subject = subject, data = data)
                    
                    ## Cap Transfer Failure Email to buyer
                    SendEmail(template = 'cto/cto_caps_transfer_failed', to = buyer['email'], subject = subject, data = data)
               
                if(transfer_status == 'CAP_TRANSFER_MADE'):
                    final_response_status = falcon.HTTP_200
                    final_response_message = {"response_code" : "200", "response_text": "Caps Transferred Successfully"}
                else:
                    final_response_status = falcon.HTTP_400
                    final_response_message = {"response_code" : "400", "response_text": cap_transfer_response["response_text"]}

            else:
                # user don't have enough caps of right type so send error and notify via email
                ## Cap Transfer Failure 
                transfer_status = 'CAP_TRANSFER_FAILED'
                subject = 'Caps transfer failed'
                # Email to seller
                data = {
                    "POSTGRE_TRAN_ID" : transaction_id
                }
                SendEmail(template = 'cto/cto_caps_transfer_failed', to = seller['email'], subject = subject, data = data)
                
                ## Cap Transfer Failure Email to buyer
                SendEmail(template = 'cto/cto_caps_transfer_failed', to = buyer['email'], subject = subject, data = data)

                final_response_status = falcon.HTTP_400
                final_response_message = {"response_code": 400, "response_text": "User does not have enough caps of right color and shape"}

            # update the transaction status 
            command = 'UPDATE coin_transfer SET status=%s WHERE id=%s'
            cur.execute(command, (transfer_status, transaction_row_id))
            conn.commit()
            cur.close()

            resp.status = final_response_status
            resp.media = final_response_message

        else:
            resp.status = falcon.HTTP_400
            resp.media = {'status' : 'Could not fetch Cap user detail'}
    
        