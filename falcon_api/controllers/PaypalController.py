import falcon
import config
import requests
import json
import base64
from string import Template

from models.caps import TransferCapsForPayment
from models.payment import GetPayment, MarkPaymentComplete, MarkTransactionComplete

import lib.paypal as paypal

# Falcon api route class for [GET : /request-payment/{id}]
class ActionStartPayment:

    def on_get(self, req, resp, id):
        filePath = './templates/paypal-button.html'
        resp.status = falcon.HTTP_200
        resp.content_type = 'text/html; charset=UTF-8'
        payment = GetPayment(id)
        if payment:
            total_amount = payment.payment_amount + payment.fee_amount
            data = {
                "PAYPAL_ENV" : config.paypal_env,
                "PAYMENT_ID" : id,
                "PAYMENT_AMOUNT" : '$' + str(total_amount)
            }
            with open(filePath, 'r') as f:
                resp.body = Template(f.read()).safe_substitute(data)
        else:
            resp.body = '<h3 style="margin-top:100px;font-family:arial;text-align:center;">Error: Payment id is Invalid</h3>'


# create paypal order payment
class ActionCreatePayment:
    def on_get(self, req, resp, id):
        payment = GetPayment(id)
        user_meta_data = json.loads(payment.user_meta_data)
        seller = user_meta_data['seller']
        seller['email'] = 'aman2talk+seller@gmail.com'

        paymentParams = {
            'paymentAmount' : payment.payment_amount,
            'currencyCode' : 'USD',
            'paymentUniqueId' : payment.payment_uniq_id,
            'sellerEmail' : seller['email'],
            'feeAmount' : payment.fee_amount,
            'feeUniqueId' : payment.fee_uniq_id
        }
        orderDetail = paypal.CreateCheckout(paymentParams)
        print("\n\n ********** Create Order detail \n\n", orderDetail, "\n\n ********* \n\n a")
        resp.media = {"orderID" : orderDetail }

# capture paypal order payment
class ActionCapturePayment:
    def on_post(self, req, resp):
        postData = req.media
        token = postData["token"]
        payerID = postData["payerID"]
        response = paypal.doExpressCheckoutPayment(token, payerID)
        
        # on successful transaction transfer caps between users
        if response['ACK'] == 'Success':
            paymentUniqueId = response['PAYMENTINFO_0_PAYMENTREQUESTID']
            MarkPaymentComplete(paymentUniqueId)
            TransferCapsForPayment(paymentUniqueId)
            MarkTransactionComplete(paymentUniqueId)
        resp.media = response

# Payment success page
class ActionPaymentSuccess:
    def on_get(self, req, resp):
        filePath = './templates/paypal-success.html'
        resp.status = falcon.HTTP_200
        resp.content_type = 'text/html; charset=UTF-8'
        with open(filePath, 'r') as f:
            resp.body = f.read()


class ActionPaypalIpn:
    def on_post(self, req, resp):

        resp.status = falcon.HTTP_200
        resp.media = {"status" : "SUCCESS"}