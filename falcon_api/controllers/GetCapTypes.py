import config
import requests
import json
from requests.auth import HTTPBasicAuth

# Falcon api route class for [GET : /get-cap-types]
class GetCapTypes:

    def on_get(self, req, resp):
        url = config.api_host + "/api/read-all"
        payload = ""
        headers = {
            'Content-Type': "application/json",
        }

        response = requests.request("GET", url, data=payload, headers=headers, auth=HTTPBasicAuth(config.api_username, config.api_password) )
        print("resp", response.text);
        jsonData = json.loads(response.text);
        resp.media = jsonData['message']['shapes'];

