
from flask import Flask, request, abort, render_template
import base64
import requests
import json
import smtplib
import urllib.parse
from email.mime.text import MIMEText

app = Flask(__name__)


@app.route('/send-mail', methods=['GET'])
def sendMail():
    if request.method == 'GET':
        # this id is not payment id but order_id from your database 
        payment_id = 'g6a5s78873g3'
        mail_body = "Payment_ID: " + payment_id + "\n" + \
                "Approve_URL: http://localhost:5000/request-payment/" + payment_id
        shootEmail("user1@yopmail.com", "Please send money", mail_body)
        return 'MAIL SENT', 200
    else:
        abort(400)




# this is the email link we will be sending in the email.
# Everything related to paypal api will start from 
@app.route('/request-payment/<id>', methods=['GET'])
def requestPayment(id):
    if request.method == 'GET':
        entries = None
        return render_template('paypal-button.html', entries=entries)
    else:
        abort(400)


#payment successful
@app.route('/payment-success', methods=['GET'])
def paymentSuccess():
    if request.method == 'GET':
        entries = None
        return render_template('paypal-success.html', entries=entries)
    else:
        abort(400)



@app.route('/payments/create-order/<id>', methods=['GET'])
def createOrder(id):
    if request.method == 'GET':
        #myToken = paypalGetToken()
        orderDetail = paypalCreateCheckout()
        print(orderDetail)
        return '{"orderID" : "' + orderDetail + '"}', 200
    else:
        abort(400)


@app.route('/payments/capture-payment', methods=['POST'])
def capturePayment():
    reqBody = request.get_json()
    response = doExpressCheckoutPayment(reqBody['token'], reqBody['payerID'])
    return json.dumps(response), 200



# IPN webhook - In case you waana implement 
@app.route('/webhook/paypal-ipn', methods=['POST'])
def ipnWebHook():
    if request.method == 'POST':
        #print(request.json)
        print("cancel")
        print(request)
        return 'SUCCESS POST', 200
    elif request.method == 'GET':
        return 'SUCCESS GET', 200
    else:
        abort(400)






def paypalCreateCheckout():
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept-Language': 'en_US'
    }
    
    rawData = {
        "USER":"", # your paypal business account email id
        "PWD":"", # your paypal password
        "SIGNATURE":"", # Your account signature
        "METHOD":"SetExpressCheckout",
        "VERSION":124.0,
        "RETURNURL":"https://example.com/return", # we don't use this. but leave it as it is
        "CANCELURL":"https://example.com/cancel", # we don't use this. but leave it as it is
        "PAYMENTREQUEST_0_PAYMENTACTION":"Sale",
        "PAYMENTREQUEST_0_AMT":2.00,
        "PAYMENTREQUEST_0_CURRENCYCODE":"USD",
        "PAYMENTREQUEST_0_DESC":"Admin Fee",
        "PAYMENTREQUEST_0_PAYMENTREQUESTID":"2728fghs", # unique random id for user 1 payment
        "PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID":"user1@yopmail.com", #user 1 paypal email
        "PAYMENTREQUEST_1_PAYMENTACTION":"Sale",
        "PAYMENTREQUEST_1_AMT":10.00,
        "PAYMENTREQUEST_1_CURRENCYCODE":"USD",
        "PAYMENTREQUEST_1_DESC":"User2 Payment",
        "PAYMENTREQUEST_1_PAYMENTREQUESTID" : "asas2728fghs", # unique random id for user 1 payment
        "PAYMENTREQUEST_1_SELLERPAYPALACCOUNTID":"user2@yopmail.com" #user 2 paypal email
    }
    
    body = rawData
    
    url = 'https://api-3t.sandbox.paypal.com/nvp'
    response = requests.post(url, headers=headers, data=body)
    
    decodedString = urllib.parse.parse_qs(response.text)
    if decodedString['TOKEN'] :
        token = decodedString['TOKEN'][0]
    else:
        token = ''
    print(token)
    return token

def doExpressCheckoutPayment(token, payerID):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept-Language': 'en_US'
    }
    
    rawData = {
        "USER":"", # your paypal business account email id
        "PWD":"", # your paypal account password
        "SIGNATURE":"", # Your account signature
        "METHOD":"DoExpressCheckoutPayment",
        "TOKEN":token,
        "PAYERID":payerID,
        "VERSION":93.0,
        "PAYMENTREQUEST_0_PAYMENTACTION":"Sale",
        "PAYMENTREQUEST_0_AMT":2.00,
        "PAYMENTREQUEST_0_CURRENCYCODE":"USD",
        "PAYMENTREQUEST_0_DESC":"Admin Fee",
        "PAYMENTREQUEST_0_PAYMENTREQUESTID":"2728fghs", #random unique id for user 1 payment
        "PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID":"user1@yopmail.com",  #user 1 paypal email
        "PAYMENTREQUEST_1_PAYMENTACTION":"Sale",
        "PAYMENTREQUEST_1_AMT":10.00,
        "PAYMENTREQUEST_1_CURRENCYCODE":"USD",
        "PAYMENTREQUEST_1_DESC":"User2 Payment",
        "PAYMENTREQUEST_1_PAYMENTREQUESTID" : "asas2728fghs",   #random unique id for user 2 payment
        "PAYMENTREQUEST_1_SELLERPAYPALACCOUNTID":"user2@yopmail.com"  #user 2 paypal email
    }
  
    body = rawData
    
    url = 'https://api-3t.sandbox.paypal.com/nvp'
    response = requests.post(url, headers=headers, data=body)
    
    decodedString = urllib.parse.parse_qs(response.text)    
    print(decodedString)
    return decodedString



def shootEmail(to, subject, body):
    gmail_user = 'dev@townsie.com'
    gmail_password = 'Oq8r8ZZfFb!'

    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(gmail_user, gmail_password)

    mytext = body
    msg = MIMEText(mytext)
    msg["From"] = gmail_user
    msg["To"] = to
    msg["Subject"] = subject

    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(gmail_user, gmail_password)
    server.sendmail(gmail_user, to, msg.as_string())
    server.close()




if __name__ == '__main__':
    app.run()